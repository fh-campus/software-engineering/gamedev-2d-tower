using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerPhysics))] // (erzwingt das Vorhandensein von diesem Script)
public class PlayerController : MonoBehaviour
{
    // (Public & SerializeField => Deutsch)
    [SerializeField] private float geschwindigkeit = 0.07f;
    [SerializeField] private float beschleunigung = 12;
    [SerializeField] private float schwerkraft = -0.0981f;
    [SerializeField] private float sprungkraft = 15f; // wie schnell ein Sprung reagiert (kleinere Werte führen zu Verzögerung/Glättung)
    [SerializeField] private float maxSprunghoehe = 2f;
    [SerializeField] private int sprungDaempfung = 20;

    // Andere Scripte / Komponenten
    private Rigidbody2D rigidBody2D;
    private PlayerPhysics playerPhysics;
    private bool lookingRightDir = true;

    // Initial keine Geschwindigkeit / Beschleunigung
    private float currentSpeed = 0f;
    private float targetSpeed;
    private Vector2 amountToMoveNow = new Vector2(0, 0);

    private float targetJumpFactor = 0f;
    private float targetJumpIncrease = 5f; // wie lang man für max sprunghöhe drücken muss (default 2f)
    private float maximumJumpIncreaseFactor = 1f;

    private bool sprungstartEingeleitet = false;
    private bool sprunghöhenErmittlung = false;
    private float maxSprunghoeheAbsolut;
    private float zielSprunghoehe = 0f;

    // Start is called before the first frame update
    void Start()
    {
        playerPhysics  = GetComponent<PlayerPhysics>();
        // controller = GetComponent<CharacterController>(); 
        // gameObject.AddComponent<CharacterController>();
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // Update is called once per frame
    void Update()
    {
        float targetSpeed = Input.GetAxis("Horizontal") * geschwindigkeit;

        // Figur hier umdrehen (nicht in Physics) wenn gerade Taste gedrückt (für nette Moonwalk Effekte, bei Walljumps)
        if (targetSpeed > 0 && !lookingRightDir ||
            targetSpeed < 0 && lookingRightDir)
            HeyMacarena();

        // Geschwindigkeitsanpassung (weiterhin) notwendig?
        if (currentSpeed != targetSpeed)
        {
            currentSpeed += Mathf.Sign(targetSpeed - currentSpeed) * beschleunigung * Time.deltaTime; // schrittweise anpassen
            // (Einmalige) Korrektur falls zu schnell!
            // TODO: Hier auch gut sofortiges umdrehen mit ähnlicher Geschwindigkeit, an Wand ggf???
            if (Mathf.Abs(currentSpeed) > Mathf.Abs(targetSpeed))
                currentSpeed = targetSpeed;

            // Unterschiedliche Vorzeichen/Richtungen => Loggen für Details
            if (currentSpeed * targetSpeed < 0) Debug.Log("Geschwindigkeit: " + currentSpeed + " | Ziel: " + targetSpeed);
        }


        // Springen ermitteln (statt Spacebar, die plattformübergreifendes/universelles Binding: Jump)
        if (Input.GetButton("Jump") && (playerPhysics.istGrundiert || sprunghöhenErmittlung))
        {

            // Erstabsprung
            if (playerPhysics.istGrundiert && !sprungstartEingeleitet) // !sprunghöhenErmittlung && targetJumpFactor == 0)
            {
                // Debug.Log("Hüüüüühüpf");
                // Folgendes würde bei einem dynamischen RigidBody gut funktionieren (kinematisch nicht!!)
                // rigidBody2d.AddForce(new Vector2(0f, sprungkraft));
                // FakeSprungkraftBewegung(); // aber wir können es faken, Dill wir es maken
                // amountToMoveNow.y += sprungkraft * Time.deltaTime;

                Debug.Log("Max Sprunghöhe: " + maxSprunghoeheAbsolut + " | Grundiert: " + playerPhysics.istGrundiert + " | SpErmittlung: " + sprunghöhenErmittlung);

                sprungstartEingeleitet = true;
                sprunghöhenErmittlung = true;
                maxSprunghoeheAbsolut = transform.position.y + maxSprunghoehe;
                targetJumpFactor = 0;
            }

            // Sprunghöhe ermitteln (Faktor von 0 bis 1)
            targetJumpFactor += targetJumpIncrease * Time.deltaTime;
            if (targetJumpFactor > maximumJumpIncreaseFactor)
            {
                targetJumpFactor = maximumJumpIncreaseFactor;
                sprunghöhenErmittlung = false; // keine weitere Sprungkrafterhöhung
            }
            //Debug.Log("Jump Factor: " + targetJumpFactor);
        }
        // Abspringen (=lange halten) unterbrochen
        else if (sprunghöhenErmittlung)
        {
            sprunghöhenErmittlung = false;
            //Debug.Log("Sprungermittlung fertig: " + targetJumpFactor + " | Maximalposition: " + maxAbsSprunghoehe);
        }

        // Bewegen
        amountToMoveNow.x = currentSpeed;
        amountToMoveNow.y += schwerkraft * Time.deltaTime; // durchgehend Schwerkraft anwenden

        // Sprung umsetzen
        if (sprungstartEingeleitet)
        {
            float absprungAbs = maxSprunghoeheAbsolut - maxSprunghoehe;
            //Debug.Log("Absprung von Höhe: " + absprungAbs + " | Sprunghöhe: " + maxSprunghoeheAbsolut*targetJumpFactor + " | Faktor: "+targetJumpFactor);
            zielSprunghoehe = (sprunghöhenErmittlung ? 1 : targetJumpFactor) * maxSprunghoehe; // während noch space gedrückt wird, mit maximum rechnen!
            //Debug.Log("Ziel Sprunghöhe: " + zielSprunghoehe+" | AbsprungAbs: "+absprungAbs);
            float sprungHoeheAbsolut = zielSprunghoehe + absprungAbs;
            float restlicheSprungDifferenz = sprungHoeheAbsolut - transform.position.y;
            Debug.Log("Restl Sprungdiff: " + restlicheSprungDifferenz);

            if (restlicheSprungDifferenz < 0) //schwerkraft*-1 && !sprunghöhenErmittlung)
            {
                amountToMoveNow.y = 0;
                sprungstartEingeleitet = false;
                sprunghöhenErmittlung = false;
                targetJumpFactor = 0;
                Debug.Log("Zielhöhe erreicht!");
            }
            else if (restlicheSprungDifferenz > zielSprunghoehe * 19/20) //((sprungDaempfung-1)/sprungDaempfung)) // nur im Beginn vom Sprung Kraft hinzufügen
                amountToMoveNow.y += restlicheSprungDifferenz * sprungkraft * Time.deltaTime;

            //if (restlicheSprungDifferenz < 0)
            //    amountToMoveNow.y = 0; // maxAbsSprunghoehe - transform.position.y;)
            //// Zielhöhe erreicht UND nicht mehr in Höhenermittlung (ganz am Anfang vom Sprung) => Fertisch gesprungen
            //if (amountToMoveNow.y + transform.position.y > zielSprunghoehe /*&& !sprunghöhenErmittlung*/) {
            //    amountToMoveNow.y = 0; // maxAbsSprunghoehe - transform.position.y;
            //    sprungstartEingeleitet = false;
            //    sprunghöhenErmittlung = false;
            //    targetJumpFactor = 0;
            //    Debug.Log("Zielhöhe erreicht!");
            //}

            //Debug.Log("Springe... Ziel: "+ zielSprunghoehe);
        }

        playerPhysics.Move(amountToMoveNow);
    }

    //bool AbsprungIstMöglich { get { return playerPhysics.istGrundiert && targetJumpFactor == 0; } }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // Richtung in die geschaut wird umdrehen
    void HeyMacarena()
    {
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        lookingRightDir = !lookingRightDir;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    //IEnumerator aktuellerFakeSprung;

    //IEnumerator AktuelleFakeSprungkraftBewegung(float sprungkraft)
    //{
    //    float i = 0.01f;
    //    while (sprungkraft > i)
    //    {
    //        rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, rigidBody2D.velocity.y + sprungkraft / i); // Für Y-Achse positive Kraft
    //        i += Time.deltaTime;
    //        yield return new WaitForEndOfFrame();
    //    }
    //    rigidBody2D.velocity = Vector2.zero;
    //    yield return null;
    //}

    //// Folgendes aufrufen um springen anzustarten
    //void FakeSprungkraftBewegung()
    //{
    //    aktuellerFakeSprung = AktuelleFakeSprungkraftBewegung(sprungkraft);
    //    StartCoroutine(aktuellerFakeSprung);
    //}
}
