using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(CameraUp))] // (erzwingt das Vorhandensein von diesem Script)
public class PlayerPhysics : MonoBehaviour
{
    // Soll von anderem Skript gelesen werden, aber nicht in Unity gesetzt/angezeigt werden
    [HideInInspector] public bool istGrundiert { get; set; } // Denglisch f�r versteckte public Werte!!!

    // Kollisionsmatrix f�r Boden und Seite, Animationslimits, etc.
    [SerializeField] private LayerMask kollissionsMatr�x;
    [SerializeField] private float geschwindigkeitDieWasAlsLaufenGilt = 0.005f;

    //private BoxCollider ourCollider;
    //private Vector3 size;
    //private Vector3 center;

    private Ray ray;
    private RaycastHit2D hit;
    private float skin = .005f;
    private bool haltStopp = false;
    private Animator tuiClubUrlaubMandy;

    // Andere Scripte
    private CameraUp cameraUpScript;
    private bool gestartet = false;

    void Start()
    {
        //ourCollider = GetComponent<BoxCollider>();
        //size = ourCollider.size;
        //center = ourCollider.center;
        tuiClubUrlaubMandy = GetComponent<Animator>();
        cameraUpScript     = GetComponent<CameraUp>();
    }

    public void Move(Vector2 moveAmount) 
    {
        float deltaX = moveAmount.x;
        float deltaY = moveAmount.y;

        // Todcheck => Falls nach unten fallend, oder vertikal gleichbleibend
        if (deltaY < 0)
        {
            // Falls gestorben => Restart the scene
            if (transform.position.y < cameraUpScript.GetOffCameraY())
                Application.LoadLevel(Application.loadedLevel);
        }

        // Erst bei bewegung Kamera starten
        if (!gestartet && deltaX != 0) {
            cameraUpScript.CameraMoveStart();
        }

        // F�r 3D auch gut, aber wir machen easi�r
        // Vector2 pp = transform.position;
        // Vector2 start = new Vector2(pp.x + center.x, pp.y + size.y);
        //float x = pp.x + center.x;
        //float y = pp.y + size.y;

        //ray = new Ray(start, Vector2.down);
        //if (Physics.Raycast(ray, out hit, Mathf.Abs(deltaY), kollissionsMatrix)) {
        //    float dst = Vector2.Distance(ray.origin, hit.point);

        //    // Stehen wir an? Dann Stopp. Ansonsten: Abstand bis auf Minimum verringern.
        //    deltaY = (dst <= skin) ? 0 : (skin - dst);

        //    // In jedem Fall sind wir jetzt ordentlich grundiert
        //    istGrundiert = true;
        //}

        // (Boden) ETWAS getroffen?
        hit = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Abs(deltaY), kollissionsMatr�x);
        //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 10, Color.green);
        if (hit.collider != null)
        {
            float dist = Mathf.Abs(hit.point.y - transform.position.y);
            // TODO: Evtl k�nnte man hier jetzt auch etwas Kraft mit D�mpfung kalkulieren/wegnehmen etc.

            // Stehen wir nicht an?  Dann Abstand bis auf Minimum verringern.  Ansonsten: Halt, Stopp!
            if (deltaY < 0) { 
                deltaY = (dist > skin) ? (skin - dist) : 0;
                istGrundiert = true;
                tuiClubUrlaubMandy.SetBool("isJump", false);
            }
        } else {
            istGrundiert = false;
            tuiClubUrlaubMandy.SetBool("isJump", true);
        }

        // (Seiten) Stehen wir evtl voll an?
        if (haltStopp) {
            if (moveAmount.x > 0 && transform.position.x > 0 || // stehen rechts an, wollen nach rechts
                moveAmount.x < 0 && transform.position.x < 0    // stehen links an, wollen nach links
            ) {
                deltaX = 0;
            } else { /* stehen zwar an, aber wollen da eh weg */ }
        }

        // Noch schnell paar Animationsparameter checken
        tuiClubUrlaubMandy.SetBool("isRun", Mathf.Abs(deltaX) >= geschwindigkeitDieWasAlsLaufenGilt);

        // Und ab die Post.
        transform.Translate( new Vector2(deltaX, deltaY) );
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    // (SEITE) Mit "etwas" kollidiert?
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Wall and Floor Tiles"))
        {
            //Debug.Log("Kollision zwischen: " + col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
            haltStopp = true;
        }
    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Wall and Floor Tiles"))
            haltStopp = false;
    }
}
