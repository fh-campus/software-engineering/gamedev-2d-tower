using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUp : MonoBehaviour
{
    private GameObject cam;
    
    public float startingMoveSpeed = 0.5f;
    public float offScreenDistance = 10.0f; // delta value of when the player is counted as completly off camera
    //private bool startet = false;

    private float currentCamSpeed;
    private float moveSpeedIncrement = 0.1f;
    private bool gestartet = false;

    void Start()
    {
        cam = GameObject.Find("Main Camera");
        currentCamSpeed = startingMoveSpeed;
    }

    void FixedUpdate()
    {
        if (!gestartet)
            return;

        // Kamera nach oben bewegen
        cam.transform.position += Vector3.up * currentCamSpeed * Time.deltaTime;
        
        // Geschwindigkeit durchgehend leicht erh�hen
        currentCamSpeed += moveSpeedIncrement * Time.deltaTime;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public float GetOffCameraY()
    {
        return cam.transform.position.y - offScreenDistance;
    }

    public void CameraMoveStart()
    {
        gestartet = true;
    }

    //public void CameraMoveStop()
    //{
    //    game
    //}
}
